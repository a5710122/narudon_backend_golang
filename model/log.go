package model

import "time"

type Log struct {
	ID         int
	Username   string
	Password   string
	IsStrong   bool
	NumOfSteps int
	CreatedAt  time.Time
}

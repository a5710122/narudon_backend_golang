package helper

import (
	"encoding/json"
	"net/http"
	"os"
	"strings"

	"project/version"

	"github.com/getsentry/sentry-go"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var log *zap.Logger

type resource struct {
	serviceName    string
	serviceVersion string
}

func (r resource) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("service.name", r.serviceName)
	enc.AddString("service.version", r.serviceVersion)
	return nil
}

func init() {
	rawJSON := []byte(`{
		"level": "info",
		"encoding": "json",
		"outputPaths": ["stdout"],
		"errorOutputPaths": ["stderr"],
		"encoderConfig": {
		"timeKey": "timestamp",
		"messageKey": "body",
		"levelKey": "severityText",
		"timeEncoder": "nanos",
		"levelEncoder": "capital"
		}
	}`)

	var cfg zap.Config
	if err := json.Unmarshal(rawJSON, &cfg); err != nil {
		panic(err)
	}

	if os.Getenv("LOG_LEVEL") == "debug" {
		cfg.Level.SetLevel(zap.DebugLevel)
	}

	var err error
	parentLog, err := cfg.Build(zap.AddCallerSkip(1))
	if err != nil {
		sentry.CaptureException(err)
		panic(err)
	}

	log = parentLog.With(
		zap.String("kind", "application"),
		zap.Object("resource", &resource{
			serviceName:    version.ServiceName,
			serviceVersion: version.ServiceVersion,
		}),
	)
}

func Info(message string, fields ...zap.Field) {
	log.Info(message, fields...)
}

func Debug(message string, fields ...zap.Field) {
	log.Debug(message, fields...)
}

func Error(message interface{}, c *gin.Context, code int, fields ...zap.Field) {
	switch v := message.(type) {
	case error:
		if code == http.StatusInternalServerError {
			sentry.CaptureException(v)
		}
		if c != nil {
			log.With(
				zap.Namespace("attributes"),
				zap.String("http.method", c.Request.Method),
				zap.Int("http.status_code", code),
				zap.String("http.path", c.Request.URL.Path),
				zap.StackSkip("stacktrace", 1),
			).Error(v.Error(), fields...)
		} else {
			log.With(
				zap.Namespace("attributes"),
				zap.StackSkip("stacktrace", 1),
			).Error(v.Error(), fields...)
		}
	case string:
		log.With(
			zap.Namespace("attributes"),
			zap.StackSkip("stacktrace", 1),
		).Error(v)
	}
}

func GinLogger() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		httplog := log.With(
			zap.Namespace("attributes"),
			zap.String("http.method", c.Request.Method),
			zap.Int("http.status_code", c.Writer.Status()),
			zap.String("http.path", c.Request.URL.Path),
		)

		switch {
		case c.Writer.Status() >= 400: // 400-499: Client error responses, 500-599: Server error responses
			var errs []string
			for _, e := range c.Errors {
				errs = append(errs, e.Error())
			}

			httplog.With(
				zap.String("errors", strings.Join(errs, "\n")),
			).Error("access_log")
		default:
			httplog.Info("access_log")
		}
	}
}

func Warn(message string, fields ...zap.Field) {
	log.Warn(message, fields...)
}

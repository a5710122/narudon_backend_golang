package repository

import (
	"project/model"
)

type LogRepository interface {
	SaveLog(log model.Log) error
}

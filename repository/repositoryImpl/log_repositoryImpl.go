package repositoryImpl

import (
	"project/model"
	"project/repository"

	"gorm.io/gorm"
)

type logRepositoryImpl struct {
	db *gorm.DB
}

func NewLogRepositoryImpl(db *gorm.DB) repository.LogRepository {
	return &logRepositoryImpl{db: db}
}

func (repo *logRepositoryImpl) SaveLog(log model.Log) error {
	tx := repo.db.Begin()

	if err := tx.Table("Log").Create(&log).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

package main

import (
	"fmt"
	"os"
	"project/handler"
	"project/helper"
	"project/repository/repositoryImpl"
	"project/service"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func main() {
	if err := sentry.Init(sentry.ClientOptions{
		Dsn:         os.Getenv("SENTRY_DSN_CMS_API"),
		Environment: os.Getenv("ENV"),
	}); err != nil {
		panic(err)
	}

	initData()
	initTimeZone()
	db := initDatabase()

	profileRepository := repositoryImpl.NewLogRepositoryImpl(db)
	profileService := service.NewProfileService(profileRepository)
	profileHandler := handler.NewProfileHandler(profileService)

	router := gin.New()
	router.Use(helper.GinLogger())
	router.Use(gin.Recovery())

	route := router.Group("/api/")
	route.Use(cors.New(cors.Config{
		AllowMethods: []string{"GET", "PUT", "POST", "PATCH", "DELETE", "OPTIONS", "HEAD"},
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{"Content-Type", "x-iims-mock", "x-api-key"},
	}))

	router.GET("/", func(c *gin.Context) {
		c.String(200, "Hello world")

	})

	route.POST("checkPassword", profileHandler.CheckPassword)

	helper.Info("Started port 3000")

	err := router.Run(":3000")
	if err != nil {
		sentry.CaptureException(err)
		helper.Error(err, nil, 0)
	}
}

func initTimeZone() {

	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		helper.Error(err, nil, 0)
	}

	time.Local = ict
}

func initDatabase() *gorm.DB {

	user := os.Getenv("DB_USER")
	pass := os.Getenv("DB_PASS")
	host := os.Getenv("DB_HOST")
	dbname := os.Getenv("DB_NAME")

	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", user, pass, host, dbname)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{Logger: logger.Default.LogMode(logger.Info)})
	if err != nil {
		sentry.CaptureException(err)
		helper.Error(err, nil, 0)
	}

	helper.Info("Database is connected")

	return db
}

func initData() {

	err := godotenv.Load()
	if err != nil {
		helper.Warn(err.Error())
	}
}

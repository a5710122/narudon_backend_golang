package service

import (
	"project/helper"
	"project/model"
	"project/repository"
	"project/service/dto"
	"unicode"

	"github.com/getsentry/sentry-go"
	"github.com/gin-gonic/gin"
)

type ProfileService interface {
	CheckPassword(c *gin.Context, userAgent dto.UserAgent) (*dto.ResponeUserAgent, error)
}

type profileService struct {
	logRepo repository.LogRepository
}

func NewProfileService(logRepository repository.LogRepository) ProfileService {
	return &profileService{logRepo: logRepository}
}

func (s profileService) CheckPassword(c *gin.Context, userAgent dto.UserAgent) (*dto.ResponeUserAgent, error) {

	IsStrong := false
	// Calculate the minimum number of steps to make the password strong
	numOfSteps := CheckStrongPassword(userAgent.Password)
	if numOfSteps <= 0 {
		IsStrong = true
	}

	newLog := model.Log{
		Username:   userAgent.Username,
		Password:   userAgent.Password,
		IsStrong:   IsStrong,
		NumOfSteps: numOfSteps,
	}

	err := s.logRepo.SaveLog(newLog)
	if err != nil {
		helper.Error(err, c, 500)
		sentry.CaptureException(err)
		return nil, err
	}

	res := dto.ResponeUserAgent{
		Username:   userAgent.Username,
		Password:   userAgent.Password,
		IsStrong:   IsStrong,
		NumOfSteps: numOfSteps,
	}

	return &res, nil
}

func CheckStrongPassword(password string) int {
	steps := 0

	// Check password length
	if len(password) < 6 {
		steps += 6 - len(password)
	} else if len(password) > 20 {
		steps += len(password) - 20
	}

	// Check for at least 1 lowercase letter, 1 uppercase letter, and 1 digit
	hasLower := false
	hasUpper := false
	hasDigit := false

	for _, char := range password {
		switch {
		case unicode.IsLower(char):
			hasLower = true
		case unicode.IsUpper(char):
			hasUpper = true
		case unicode.IsDigit(char):
			hasDigit = true
		}
	}

	if !hasLower {
		steps++
	}
	if !hasUpper {
		steps++
	}
	if !hasDigit {
		steps++
	}

	// Check for repeating characters
	for i := 0; i < len(password)-2; i++ {
		if password[i] == password[i+1] && password[i] == password[i+2] {
			steps++
			break
		}
	}

	return steps
}

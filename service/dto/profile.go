package dto

type UserAgent struct {
	Username string `json:"userName" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type ResponeUserAgent struct {
	Username   string `json:"userName"`
	Password   string `json:"password"`
	IsStrong   bool   `json:"isStrong"`
	NumOfSteps int    `json:"numOfSteps"`
}

CREATE TABLE `Log` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(45) NOT NULL,
    `password` VARCHAR(45) NOT NULL,
    `is_strong` TINYINT NOT NULL,
    `num_of_steps` INT NULL,
    `created_at` DATETIME NULL,
    PRIMARY KEY (`id`));
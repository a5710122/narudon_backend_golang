package handler

import (
	"net/http"
	"project/service"
	"project/service/dto"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

var validate = validator.New()

type ProfileHandler interface {
	CheckPassword(c *gin.Context)
}

type profilehandler struct {
	profileService service.ProfileService
}

func NewProfileHandler(profileService service.ProfileService) ProfileHandler {
	return profilehandler{profileService: profileService}
}

func (h profilehandler) CheckPassword(c *gin.Context) {

	userAgent := &dto.UserAgent{}
	if err := c.BindJSON(userAgent); err != nil {
		Error(c, err)
		return
	}

	if err := validate.Struct(userAgent); err != nil {
		Error(c, err)
		return
	}

	res, err := h.profileService.CheckPassword(c, *userAgent)
	if err != nil {
		Error(c, err)
		return
	}

	c.JSON(http.StatusCreated, res)
}

package handler

import (
	"net/http"
	"project/helper"

	"github.com/gin-gonic/gin"
)

type ErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type Response struct {
	Success bool   `json:"success"`
	Error   string `json:"error,omitempty"`
}

type ErrorMsg struct {
	Field   string `json:"field"`
	Message string `json:"message"`
}

type ErrorValidateResponse struct {
	Code   int        `json:"code"`
	Errors []ErrorMsg `json:"errors"`
}

func ValidateError(c *gin.Context, err error) {
	helper.Error(err, c, http.StatusBadRequest)
	c.JSON(http.StatusBadRequest, Response{Success: false, Error: err.Error()})
}

func Error(c *gin.Context, err error) {
	if err == nil {
		return
	}
	
	helper.Error(err, c, http.StatusInternalServerError)
	c.JSON(http.StatusInternalServerError, Response{Success: false, Error: err.Error()})
}

package handler_test

import (
	"bytes"

	// "errors"
	"net/http"
	"net/http/httptest"
	"project/handler"
	"project/service/dto"
	"project/service/mocks"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

// var mission string = "mission"
// var testName string = "test"
// var total int = 10
// var now time.Time = time.Now()
// var unexpectedErr string = "unexpected error"

func TestCheckPassword(t *testing.T) {
	profileServ := new(mocks.ProfileService)
	h := handler.NewProfileHandler(profileServ)

	bodyJsonFail := `{
		"username": "testUsername",
		"password": ""
	}`

	bodyJsonSuccess := `{
		"username": "testUsername",
		"password": "Test@got657"
	}`

	bodyDto := dto.UserAgent{
		Username: "testUsername",
		Password: "Test@got657",
	}

	response := dto.ResponeUserAgent{
		Username:   "testUsername",
		Password:   "Test@got657",
		IsStrong:   true,
		NumOfSteps: 0,
	}

	t.Run("bind json error", func(t *testing.T) {
		w := httptest.NewRecorder()
		gin.SetMode(gin.TestMode)
		c, r := gin.CreateTestContext(w)

		r.POST("/checkPassword", h.CheckPassword)

		mockInvalidJsonReq := []byte(`{"title": "test1",}`)
		bodyReader := bytes.NewReader(mockInvalidJsonReq)
		c.Request, _ = http.NewRequest(http.MethodPost, "/checkPassword", bodyReader)
		r.ServeHTTP(w, c.Request)
		defer w.Result().Body.Close()

		if assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode) {
			mockErrorString := `{"success":false,"error":"invalid character '}' looking for beginning of object key string"}`
			assert.Equal(t, mockErrorString, w.Body.String())
		}
	})

	t.Run("validate error", func(t *testing.T) {
		w := httptest.NewRecorder()
		gin.SetMode(gin.TestMode)
		c, r := gin.CreateTestContext(w)

		r.POST("/checkPassword", h.CheckPassword)

		mockInvalidJsonReq := []byte(bodyJsonFail)
		bodyReader := bytes.NewReader(mockInvalidJsonReq)
		c.Request, _ = http.NewRequest(http.MethodPost, "/checkPassword", bodyReader)
		r.ServeHTTP(w, c.Request)
		defer w.Result().Body.Close()

		if assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode) {
			mockErrorString := `{"success":false,"error":"Key: 'UserAgent.Password' Error:Field validation for 'Password' failed on the 'required' tag"}`
			assert.Equal(t, mockErrorString, w.Body.String())
		}
	})

	t.Run("success", func(t *testing.T) {
		w := httptest.NewRecorder()
		gin.SetMode(gin.TestMode)
		c, r := gin.CreateTestContext(w)

		r.POST("/checkPassword", h.CheckPassword)

		mockValidJsonReq := []byte(bodyJsonSuccess)
		bodyReader := bytes.NewReader(mockValidJsonReq)

		profileServ.On("CheckPassword", mock.AnythingOfType("*gin.Context"), bodyDto).Return(&response, nil).Once()

		c.Request, _ = http.NewRequest(http.MethodPost, "/checkPassword", bodyReader)
		r.ServeHTTP(w, c.Request)
		defer w.Result().Body.Close()

		if assert.Equal(t, http.StatusCreated, w.Result().StatusCode) {
			res := `{
				"userName": "testUsername",
				"password": "Test@got657",
				"isStrong": true,
				"numOfSteps": 0
			}`
			assert.JSONEq(t, res, w.Body.String())
		}
	})

}
